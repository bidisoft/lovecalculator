package com.example.seb.lovecalculator;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONObject;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    EditText firstName;
    EditText secondName;
    ProgressBar bar;
    ImageButton button;
    public static LoveCalculator lc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageButton action = (ImageButton) findViewById(R.id.imageButton);
        action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculateLove(((EditText) findViewById(R.id.firstNameEditText)).getText().toString(), ((EditText) findViewById(R.id.secondNameeditText)).getText().toString());
            }
        });
    }

    private void calculateLove(String firstName, String secondName) {
        LoveCalculatorTask lct = new LoveCalculatorTask();
        lct.execute(firstName, secondName);
    }

    private class LoveCalculatorTask extends AsyncTask<String, Void, LoveCalculator> {
        LoveCalculatorTask() {

        }

        @Override
        protected LoveCalculator doInBackground(String... params) {
            RestClient rc = new RestClient("https://love-calculator.p.mashape.com/getPercentage");
            rc.addHeader("X-Mashape-Key", "t9UvtPKJiKmshv7eUo5mzdAFIxbOp108kVRjsnvKF037Z2XRme");
            rc.addHeader("Accept", "application/json");
            rc.addParam("fname", params[0]);
            rc.addParam("sname", params[1]);

            try {
                rc.execute(RestClient.RequestMethod.GET);
                lc = new LoveCalculator(new JSONObject(rc.getResponse()));
                return lc;
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(LoveCalculator lc) {
            bar = (ProgressBar) findViewById(R.id.progressBar);
            button = (ImageButton) findViewById(R.id.imageButton);

            button.setVisibility(View.INVISIBLE);
            bar.setVisibility(View.VISIBLE);

            // jump
            Intent intent = new Intent(MainActivity.this, ResultActivity.class);
            startActivity(intent);
        }

    }
}

package com.example.seb.lovecalculator;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONObject;

public class ResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        LoveCalculator lc = MainActivity.lc;
        ((TextView) findViewById(R.id.firstNameTextView)).setText(lc.getFname());
        ((TextView) findViewById(R.id.secondNameTextView)).setText(lc.getSname());
        ((TextView) findViewById(R.id.resultTextView)).setText(lc.getResult());
        ((TextView) findViewById(R.id.percentageTextView)).setText(lc.getPercentage().concat(" %"));

        ImageButton restart = ((ImageButton) findViewById(R.id.restartButton));
        ImageView matchImage = ((ImageView) findViewById(R.id.matchImage));
        int value = Integer.valueOf(lc.getPercentage());
        if (value < 40) {
            matchImage.setImageResource(R.drawable.heartbroken);
        } else if (value >= 40 && value <= 80) {
            matchImage.setImageResource(R.drawable.heart);
        } else {
            matchImage.setImageResource(R.drawable.inlove);
        }

        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResultActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

    }

}

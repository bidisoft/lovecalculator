package com.example.seb.lovecalculator;

import org.json.*;

import java.io.Serializable;


public class LoveCalculator implements Serializable {

    public static String S_RESULT_JSON_KEY_FNAME = "fname";
    public static String S_RESULT_JSON_KEY_RESULT = "result";
    public static String S_RESULT_JSON_KEY_PERCENTAGE = "percentage";
    public static String S_RESULT_JSON_KEY_SNAME = "sname";
	
    private String fname;
    private String result;
    private String percentage;
    private String sname;
    
    
	public LoveCalculator() {
        this.fname = "";
        this.result = "";
        this.percentage = "";
        this.sname = "";
	}	
        
    public LoveCalculator(JSONObject json) {
    
        this.fname = json.optString(S_RESULT_JSON_KEY_FNAME);
        this.result = json.optString(S_RESULT_JSON_KEY_RESULT);
        this.percentage = json.optString(S_RESULT_JSON_KEY_PERCENTAGE);
        this.sname = json.optString(S_RESULT_JSON_KEY_SNAME);

    }
    
    public String getFname() {
        return this.fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getResult() {
        return this.result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getPercentage() {
        return this.percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getSname() {
        return this.sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }


    
}
